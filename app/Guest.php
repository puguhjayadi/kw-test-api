<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
	protected $fillable = ['name', 'address', 'latitude', 'longitude'];

	protected $hidden = ['created_at','updated_at'];
	
	public function orders()
	{
		return $this->hasMany('App\Order');
	}
}
