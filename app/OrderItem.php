<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
	protected $fillable = ['order_id', 'room_id', 'quantity', 'price'];
	
	protected $appends = ['total_price', 'name'];

	public function getTotalPriceAttribute()
	{
		return ($this->price * $this->quantity);    
	}

	public function getNameAttribute()
	{
		return $this->room()->pluck('name')->first();
	}

	public function order()
	{
		return $this->belongsTo('App\Order');
	}

	public function room()
	{
		return $this->belongsTo('App\Room');
	}
}