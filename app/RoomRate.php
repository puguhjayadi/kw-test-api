<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomRate extends Model
{
	protected $fillable = ['room_id', 'date', 'price'];
	
	public function room()
	{
		return $this->belongsTo('App\Room');
	}
}
