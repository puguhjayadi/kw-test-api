<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomAvailability extends Model
{
	protected $fillable = ['room_id', 'date', 'quantity'];
	
	public function room()
	{
		return $this->belongsTo('App\Room');
	}
}
