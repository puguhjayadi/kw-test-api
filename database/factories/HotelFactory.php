<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hotel;
use Faker\Generator as Faker;

$factory->define(Hotel::class, function (Faker $faker) {
	return [
		'name' => $faker->name,
		'address' => $faker->address,
		'latitude' => rand(0,10),
		'longitude' => rand(0,10),
    ];
});
