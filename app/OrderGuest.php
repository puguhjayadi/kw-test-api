<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderGuest extends Model
{
	protected $fillable = ['order_id', 'name', 'email', 'phone'];

	public function order()
	{
		return $this->belongsTo('App\Order');
	}
}
