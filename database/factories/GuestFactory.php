<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Guest;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Guest::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
		'phone' => $faker->phoneNumber,
        'identification_id' => Str::random(10),
    ];
});
