<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\Hotel;
use DB;

class ReportController extends Controller
{
	public function order()
	{
		$month  = optional(request())->month ?? date('m');
		$year   = optional(request())->year ?? date('Y');
		$hotel  = optional(request())->hotel ?? 1;
		$group_by  = optional(request())->group_by ?? 'day';


		$validator = \Validator::make(['hotel' => $hotel], [
			'hotel' => 'required|numeric|exists:hotels,id'
		]);

		if ($validator->fails()) {
			return response()->json(['message' => $validator->messages()], 422);
		}

		switch (request()->group_by) {
			case 'year':
			$groupBy = "%Y";
			break;

			case 'month':
			$groupBy = "%Y-%m";
			break;

			default:
			$groupBy = "%Y-%m-%d";
			break;
		}

		$results = Order::select( DB::raw("DATE_FORMAT(orders.created_at,'".$groupBy."') AS dates") )
		->addSelect(DB::raw('COUNT(orders.id) as orders'))
		->whereMonth('orders.created_at', '=', $month)
		->whereYear('orders.created_at', '=', $year)
		->where('orders.hotel_id', '=', $hotel)
		->groupBy( DB::raw("DATE_FORMAT(orders.created_at,'".$groupBy."')") )
		->orderBy( DB::raw("DATE_FORMAT(orders.created_at,'".$groupBy."')") )
		->get();

		DB::beginTransaction();
		try {

			$orders = [];
			if(count($results)){
				foreach ($results as $key => $value) {
					$orders[] = [
						'dates' => $value->dates,
						'orders' => $value->orders
					];
				}
			} else {
				return response()->json(['message' => 'empty' ]);
			}

			$hotel = Hotel::find($hotel);

			$results = [
				'status' => 200,
				'message' => "Success Query All Orders",
				'year' => $year,
				'month' => $month,
				'group_by' => $group_by,
				'data' => $orders,
				'hotel' => $hotel,
			];
			DB::commit();
			return response()->json($results);

		} catch(\Exception $e){

			DB::rollback();
			return response()->json(['message' => $e->getMessage() ]);
		}
	}
}
