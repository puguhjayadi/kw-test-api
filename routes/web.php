<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::get('api/v1/hotels/{hotel_id}/rooms', 'API\RoomController@rooms');
Route::get('api/v1/hotels/{hotel_id}/rooms/availiabilites', 'API\RoomController@availiabilites');
Route::get('api/v1/hotels/{hotel_id}/rooms/rates', 'API\RoomController@rates');

Route::resource('api/v1/hotels', 'API\HotelController');
Route::resource('api/v1/orders', 'API\OrderController');
Route::get('api/v1/reports/orders', 'API\ReportController@order');

