<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Room;
use Faker\Generator as Faker;

$factory->define(Room::class, function (Faker $faker) {
    return [
		'hotel_id' => 1,
		'name' => 'Room '.$faker->randomElement(['A','B','C','D','E']),
		'quantity' => rand(1, 5),
		'price' => rand(100000, 500000),
    ];
});
