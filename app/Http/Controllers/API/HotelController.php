<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Hotel;
use DB;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
        http://127.0.0.1:8000/api/v1/hotels?per_page=10&page=1&order_by=asc&sort_by=created_at&search=
        */

        $per_page  = optional(request())->per_page ?? 1;
        $page      = optional(request())->page ?? 1;
        $sort_by   = optional(request())->sort_by ?? 'created_at';
        $order_by  = optional(request())->order_by ?? 'asc';
        $search    = optional(request())->search ?? null;

        DB::beginTransaction();

        try {

            $hotels = Hotel::where('name', 'LIKE', '%'.$search.'%')
            ->orWhere('address', 'LIKE', '%'.$search.'%')
            ->orderBy($sort_by, $order_by)->paginate($per_page);

            $response = [
                'status' => 200,
                'message' => "Success Query All Hotels",
                'per_page' => $per_page,
                'page' => $page,
                'order_by' => $order_by,
                'sort_by' => $sort_by,
                'search' => $search,
                'data' => $hotels->items(),
            ];

            DB::commit();
            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();

        try {

            Hotel::create($request->all());

            $response = [
                'status' => 200,
                'message' => $request['name']." is successfully created",
                'data' => $request->all(),
            ];

            DB::commit();
            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        DB::beginTransaction();

        try {

         $validator = \Validator::make(['hotel_id' => $id], [
            'hotel_id' => 'required|numeric|exists:hotels,id'
        ]);

         if ($validator->fails()) {
            return response()->json(['message' => $validator->messages()], 422);
        }

        $result = Hotel::find($id);

        $response = [
            'status' => 200,
            'message' => $result->name." is successfully retrieved",
            'data' => $result,
        ];

        DB::commit();
        return response()->json($response, 200);

    } catch(\Exception $e){

        DB::rollback();
        return response()->json(['message' => $e->getMessage() ]);
    }

}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $validator = \Validator::make(['hotel_id' => $id], [
                'hotel_id' => 'required|numeric|exists:hotels,id'
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->messages()], 422);
            }

            Hotel::whereId($id)->update($request->all());

            $response = [
                'status' => 200,
                'message' => $request['name']." is successfully updated",
                'data' => $request->all(),
            ];

            DB::commit();
            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        DB::beginTransaction();

        try {

            $validator = \Validator::make(['hotel_id' => $id], [
                'hotel_id' => 'required|numeric|exists:hotels,id'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => $validator->messages()], 422);
            }
            $data = Hotel::findOrFail($id);
            $data->delete();
           
            $response = [
                'status' => 200,
                'message' => $data->name." is successfully deleted",
            ];

            DB::commit();
            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }
    }
}
