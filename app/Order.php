<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = ['hotel_id', 'guest_id', 'status', 'checkin', 'checkout'];

	public function orderItems()
	{
		return $this->hasMany('App\OrderItem');
	}

	public function orderCustomer()
	{
		return $this->hasMany('App\OrderCustomer');
	}

	public function hotel()
	{
		return $this->belongsTo('App\Hotel');
	}

	public function customer()
	{
		return $this->belongsTo('App\Customer');
	}
}
