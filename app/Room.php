<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
	protected $fillable = ['hotel_id', 'name', 'quantity', 'price'];
	
	protected $hidden = ['created_at','updated_at'];
	
	public function rates()
	{
		return $this->hasMany('App\RoomRate');
	}

	public function availabilities()
	{
		return $this->hasMany('App\RoomAvailability');
	}

	public function orderItems()
	{
		return $this->hasMany('App\OrderItem');
	}

	public function hotel()
	{
		return $this->belongsTo('App\Hotel');
	}
}
