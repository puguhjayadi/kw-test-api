<?php

use Illuminate\Database\Seeder;

class RoomAvailabilityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$room_id = App\Room::all();
    	foreach($room_id as $k=>$v) {
    		for($i=1; $i<=count($room_id); $i++) {
    			DB::table("room_availabilities")->insert(
    				[
    					'room_id' => $v->id,
    					'date' => date('Y-m')."-".$i,
    					'quantity' => rand(10,30),
    				]
    			);
    		}
    	}


    }
}
