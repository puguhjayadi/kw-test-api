<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Hotel;
use App\Guest;
use App\Order;
use App\OrderGuest;
use App\OrderItem;
use App\Room;
use App\RoomRate;
use DB;

class OrderController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{

    DB::beginTransaction();

    try {

        /*
        http://127.0.0.1:8000/api/v1/orders?per_page=10&page=1&sort_by=created_at&order_by=asc&guest=1%27
        */
        $per_page  = optional(request())->per_page ?? 1;
        $page      = optional(request())->page ?? 1;
        $sort_by   = optional(request())->sort_by ?? 'created_at';
        $order_by  = optional(request())->order_by ?? 'asc';
        $guest     = optional(request())->guest ?? 1;

        $orders = Order::orderBy($sort_by, $order_by)->paginate($per_page);

        $orders->groupBy(function ($orders) {
            $orders['total_price']  = $orders->orderItems->sum('total_price');
            $orders['rooms']        = $orders->orderItems;
            $orders['hotel']        = $orders->hotel;
        });

        $orders->transform(function($value) {

            foreach ($value->rooms as $room){
                $room->makeHidden(['id', 'order_id', 'created_at', 'updated_at']);
            }

            return $value->only([
                'id', 
                'checkin',
                'checkout',
                'total_price', 
                'status', 
                'rooms', 
                'hotel',
                'created_at',
                'updated_at',
            ]);
        });

        $guestData = Guest::find($guest);
        $guestData->makeHidden(['identification_id']);
        
        $results = [
            'status' => 200,
            'message' => "Success Query All Orders",
            'per_page' => $per_page,
            'page' => $page,
            'order_by' => $order_by,
            'sort_by' => $sort_by,
            'data' => $orders->all(),
            'guest' => $guestData,
        ];

        DB::commit();
        return response()->json($results);

    } catch(\Exception $e){

        DB::rollback();
        return response()->json(['message' => $e->getMessage() ]);
    }

}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
    $post = $request->all();

    $validator = \Validator::make(
        [
            'hotel_id' => $post['hotel_id'],
            'guest_id' => $post['guest_id']
        ],
        [
            'hotel_id' => 'required|numeric|exists:hotels,id',
            'guest_id' => 'required|numeric|exists:guests,id'
        ]
    );

    if ($validator->fails()) {
        return response()->json(['message' => $validator->messages()], 422);
    }

    DB::beginTransaction();

    try {

        $hotel      = Hotel::findOrFail($post['hotel_id']);
        
        $checkin    = $post['checkin'];
        $checkout   = $post['checkout'];

        $order = [
            'hotel_id' => $post['hotel_id'],
            'checkin' => $checkin,
            'checkout' => $checkout,
            'status' => 'pending',
            'guest_id' => $post['guest_id'],
        ];

        $orderPost = Order::create($order);
        $order_id = $orderPost->id;

        $guest      = Guest::find($post['guest_id'])->toArray();
        unset($guest['created_at'],  $guest['updated_at'], $guest['identification_id']);

        $orderGuest = $guest;
        $orderGuest['order_id'] = $order_id;

        OrderGuest::create($orderGuest);

        $total_price = 0;
        $tmpItems = [];

        $results = RoomRate::distinct()->rightJoin('rooms', function($join) use ($checkin, $checkout)    
        {
            $join->on('rooms.id', '=', 'room_rates.room_id');
            $join->on('room_rates.date','>=',DB::raw("'".$checkin."'"));
            $join->on('room_rates.date','<=',DB::raw("'".$checkout."'"));
        })
        ->addSelect('rooms.id as room_id')
        ->addSelect(DB::raw('rooms.name, IF(room_rates.price IS NULL, rooms.price, room_rates.price) as price'))
        ->get();

        foreach ($results as $k => $v) {

            foreach ($post['rooms'] as $key => $value) {

                if($v->room_id == $value['room_id']){

                    $key = \Validator::make(['room_id' => $value['room_id'] ], [
                        'room_id' => 'required|numeric|exists:rooms,id'
                    ]);

                    if ($key->fails()) {
                        return response()->json(['message' => $key->messages()], 422);
                    }

                    $item = [
                        'order_id' => $order_id,
                        'room_id' => $value['room_id'],
                        'quantity' => $value['quantity'],
                        'price' => $v->price,
                        'price' => $v->price,
                    ];
                    $orderItem = OrderItem::create($item);

                    $tmpItems[] = [
                        'id' => $orderItem->id,
                        'name' => $v->name,
                        'room_id' => $value['room_id'],
                        'quantity' => $value['quantity'],
                        'price' => $v->price,
                        'total_price' => ($v->price * $value['quantity']),
                    ];

                    $total_price += ($v->price * $value['quantity']);
                }
            }
        }
        DB::commit();

        $response['status'] = 200;
        $response['message'] = "Success Query All Orders";
        $response['data'] = [
            'id' => $order_id,
            'total_price' => $total_price,
            'status' => $orderPost->status,
            'checkin' => $checkin,
            'checkout' => $checkout,
            'hotel' => $hotel,
            'rooms' => $tmpItems,
        ];
        $response['guest'] = $guest;

        return response()->json($response);

    } catch(\Exception $e){

        DB::rollback();
        return response()->json(['message' => $e->getMessage() ]);
    }
}

public function show($order_id){

    $validator = \Validator::make(
        [
            'order_id' => $order_id,
        ],
        [
            'order_id' => 'required|numeric|exists:orders,id',
        ]
    );

    if ($validator->fails()) {
        return response()->json(['message' => $validator->messages()], 422);
    }

    DB::beginTransaction();

    try {

        $orders = Order::find($order_id);

        $orders['total_price']  = $orders->orderItems->sum('total_price');
        $rooms = $orders->orderItems;
        $hotel = $orders->hotel;

        $orders['hotel'] = $hotel;

        foreach ($rooms as $room){
            $room->makeHidden(['id', 'order_id', 'created_at', 'updated_at']);
        }
        $orders['rooms'] = $rooms;

        $data = $orders->only([
            'id', 
            'checkin',
            'checkout',
            'status', 
            'total_price', 
            'hotel',
            'rooms', 
        ]);

        $guestData = Guest::find($orders->guest_id);
        $guestData->makeHidden(['identification_id']);
        
        $results = [
            'status' => 200,
            'message' => "Success Query Detail Order",
            'data' => $data,
            'guest' => $guestData,
        ];

        DB::commit();
        return response()->json($results);


    } catch(\Exception $e){

        DB::rollback();
        return response()->json(['message' => $e->getMessage() ]);
    }

}


}
