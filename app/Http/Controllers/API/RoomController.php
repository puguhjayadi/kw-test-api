<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Room;
use App\RoomRate;
use App\RoomAvailability;
use App\Hotel;
use DB;

class RoomController extends Controller
{

# http://127.0.0.1:8000/api/v1/hotels/1/rooms?per_page=10&page=1&order_by=asc&sort_by=created_at&search=room
    public function rooms($hotel_id)
    {

        $validator = \Validator::make(['hotel_id' => $hotel_id], [
            'hotel_id' => 'required|numeric|exists:hotels,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages()], 422);
        }

        $per_page  = optional(request())->per_page ?? 10;
        $page      = optional(request())->page ?? 1;
        $sort_by   = optional(request())->sort_by ?? 'created_at';
        $order_by  = optional(request())->order_by ?? 'asc';
        $search    = optional(request())->search ?? null;

        DB::beginTransaction();

        try {

            $hotel  = Hotel::find($hotel_id);

            $rooms  = Room::where('hotel_id', $hotel_id)
            ->where('name', 'LIKE', '%'.$search.'%')
            ->orderBy($sort_by, $order_by)->paginate($per_page);

            $data  = [];

            foreach ($rooms as $room) {
                $data[] = [
                    'id' => $room->id,
                    'name' => $room->name,
                    'quantity' => $room->quantity,
                    'price' => $room->price,
                ];
            }

            $results = [
                'status' => 200,
                'message' => "Success Query All Room Rates",
                'per_page' => $per_page,
                'page' => $page,
                'order_by' => $order_by,
                'sort_by' => $sort_by,
                'search' => $search,
                'data' => $data,
                'hotel' => $hotel,
            ];

            DB::commit();

            return response()->json($results, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }

    }

# http://127.0.0.1:8000/api/v1/hotels/1/rooms/rates?month=2&year=2020
    public function rates($hotel_id)
    {
        $validator = \Validator::make(['hotel_id' => $hotel_id], [
            'hotel_id' => 'required|numeric|exists:hotels,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages()], 422);
        }

        $month  = optional(request())->month ?? date('m');
        $year   = optional(request())->year ?? date('Y');
        $per_page  = optional(request())->per_page ?? 1;
        $page      = optional(request())->page ?? 1;

        DB::beginTransaction();

        try {

            $hotel  = Hotel::find($hotel_id);

            $rooms  = Room::where('hotel_id', $hotel_id)
            ->where('hotel_id', $hotel_id)
            ->orderBy('id')
            ->paginate($per_page);

            $roomRatesMonthYear = RoomRate::whereYear('date', '=', $year)->whereMonth('date', '=', $month)->get();

            $roomRates = count($roomRatesMonthYear) ? $roomRatesMonthYear :  RoomRate::all();

            $data  = [];

            foreach ($rooms as $room) {
                $tmpRate           = [];
                $filteredRoomRates = $roomRates->where('room_id', $room->id)
                ->groupBy(static function ($val) {
                    return \Carbon\Carbon::parse($val->date)->format('Y-m');
                });

                foreach ($filteredRoomRates as $key => $roomRate) {

                    $daysInMonth = cal_days_in_month(0, $month, $year);

                    for ($i = 1; $i <= $daysInMonth; $i++) {
                        $date         = \Carbon\Carbon::parse($year."-".$month."-".$i)->format('Y-m-d');                   
                        $currRoomRate = $roomRate->where('date', $date)->first();
                        $tmpRate[]    = [
                            'date'  => $date,
                            'price' => optional($currRoomRate)->price ?? $room->price,
                        ];
                    }
                }

                $data[] = [
                    'id'     => $room->id,
                    'name'     => $room->name,
                    'quantity'     => $room->quantity,
                    'price'     => $room->price,
                    'rates'     => $tmpRate,
                ];
            }

            $results = [
                'status' => 200,
                'message' => "Success Query Room Availiable",
                'per_page' => $per_page,
                'page' => $page,
                'year' => $year,
                'month' => $month,
                'data' => $data,
                'hotel' => $hotel,
            ];

            DB::commit();

            return response()->json($results, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }
    }

# http://127.0.0.1:8000//api/v1/hotels/1/rooms/availiabilites?month=2&year=2020
    public function availiabilites($hotel_id)
    {

        $validator = \Validator::make(['hotel_id' => $hotel_id], [
            'hotel_id' => 'required|numeric|exists:hotels,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages()], 422);
        }

        $month  = optional(request())->month ?? date('m');
        $year   = optional(request())->year ?? date('Y');
        $per_page  = optional(request())->per_page ?? 1;
        $page      = optional(request())->page ?? 1;

        DB::beginTransaction();

        try {

            $hotel  = Hotel::find($hotel_id);

            $rooms  = Room::where('hotel_id', $hotel_id)
            ->where('hotel_id', $hotel_id)
            ->orderBy('id')
            ->paginate($per_page);

            $roomAvailiabilitesMonthYear = RoomAvailability::whereYear('date', '=', $year)->whereMonth('date', '=', $month)->get();

            $roomAvailiabilites = count($roomAvailiabilitesMonthYear) ? $roomAvailiabilitesMonthYear :  RoomAvailability::all();

            $data  = [];

            foreach ($rooms as $room) {
                $tmpRate           = [];
                $filteredRoomAvailiabilites = $roomAvailiabilites->where('room_id', $room->id)
                ->groupBy(static function ($val) {
                    return \Carbon\Carbon::parse($val->date)->format('Y-m');
                });

                foreach ($filteredRoomAvailiabilites as $key => $roomAvailability) {

                    $daysInMonth = cal_days_in_month(0, $month, $year);

                    for ($i = 1; $i <= $daysInMonth; $i++) {
                        $date         = \Carbon\Carbon::parse($year."-".$month."-".$i)->format('Y-m-d');                   
                        $currRoomAvailiabilites = $roomAvailability->where('date', $date)->first();
                        $tmpRate[]    = [
                            'date'  => $date,
                            'quantity' => optional($currRoomAvailiabilites)->quantity ?? $room->quantity,
                        ];
                    }
                }

                $data[] = [
                    'id'     => $room->id,
                    'name'     => $room->name,
                    'quantity'     => $room->quantity,
                    'price'     => $room->price,
                    'rates'     => $tmpRate,
                ];
            }

            $results = [
                'status' => 200,
                'message' => "Success Query Room Availiable",
                'per_page' => $per_page,
                'page' => $page,
                'year' => $year,
                'month' => $month,
                'data' => $data,
                'hotel' => $hotel,
            ];
            DB::commit();

            return response()->json($results, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }

    }
}
