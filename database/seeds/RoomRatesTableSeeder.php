<?php

use Illuminate\Database\Seeder;

class RoomRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$room_id = App\Room::all();
    	foreach($room_id as $k=>$v) {
            for($i=1; $i<=10; $i++) {
    		// for($i=1; $i<=\Carbon\Carbon::now()->daysInMonth; $i++) {
    			DB::table("room_rates")->insert(
    				[
    					'room_id' => $v->id,
    					'date' => date('Y-m')."-".$i,
    					'price' => rand(1000,9999),
    				]
    			);
    		}
    	}
    }
}
