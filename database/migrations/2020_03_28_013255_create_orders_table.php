<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hotel_id');
            $table->unsignedBigInteger('guest_id')->nullable();
            $table->enum("status",['pending','paid','ready',"checkin","checkout","expired","canceled"])->default("pending");;
            $table->datetime('checkin')->nullable();
            $table->datetime('checkout')->nullable();
            $table->timestamps();
            
            $table->foreign('hotel_id')
                ->references('id')->on('hotels')
                ->onDelete('cascade');

            $table->foreign('guest_id')
                ->references('id')->on('guests')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
